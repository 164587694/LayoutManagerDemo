package com.example.layoutmanagerdemo.detach;

import androidx.annotation.NonNull;

public interface RecyclerListener {
    void onViewRecycled(@NonNull ViewHolder holder);
}