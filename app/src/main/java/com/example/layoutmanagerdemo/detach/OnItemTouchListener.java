package com.example.layoutmanagerdemo.detach;

import android.view.MotionEvent;

import androidx.annotation.NonNull;

public interface OnItemTouchListener {
    boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e);

    void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e);

    void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept);
}