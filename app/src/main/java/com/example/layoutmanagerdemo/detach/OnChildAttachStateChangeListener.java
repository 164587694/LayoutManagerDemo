package com.example.layoutmanagerdemo.detach;

import android.view.View;

import androidx.annotation.NonNull;

public interface OnChildAttachStateChangeListener {
    void onChildViewAttachedToWindow(@NonNull View view);

    void onChildViewDetachedFromWindow(@NonNull View view);
}