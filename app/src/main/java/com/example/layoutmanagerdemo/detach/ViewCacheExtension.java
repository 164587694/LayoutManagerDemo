package com.example.layoutmanagerdemo.detach;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class ViewCacheExtension {
    @Nullable
    public abstract View getViewForPositionAndType(@NonNull Recycler recycler, int position,
                                                   int type);
}