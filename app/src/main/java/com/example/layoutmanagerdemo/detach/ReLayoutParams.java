package com.example.layoutmanagerdemo.detach;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class ReLayoutParams extends android.view.ViewGroup.MarginLayoutParams {
    ViewHolder mViewHolder;
    final Rect mDecorInsets = new Rect();
    boolean mInsetsDirty = true;
    boolean mPendingInvalidate = false;

    public ReLayoutParams(Context c, AttributeSet attrs) {
        super(c, attrs);
    }

    public ReLayoutParams(int width, int height) {
        super(width, height);
    }

    public ReLayoutParams(ViewGroup.MarginLayoutParams source) {
        super(source);
    }

    public ReLayoutParams(ViewGroup.LayoutParams source) {
        super(source);
    }

    public ReLayoutParams(ReLayoutParams source) {
        super((ViewGroup.LayoutParams) source);
    }

    public boolean viewNeedsUpdate() {
        return mViewHolder.needsUpdate();
    }

    public boolean isViewInvalid() {
        return mViewHolder.isInvalid();
    }

    public boolean isItemRemoved() {
        return mViewHolder.isRemoved();
    }

    public boolean isItemChanged() {
        return mViewHolder.isUpdated();
    }

    @Deprecated
    public int getViewPosition() {
        return mViewHolder.getPosition();
    }

    public int getViewLayoutPosition() {
        return mViewHolder.getLayoutPosition();
    }

    public int getViewAdapterPosition() {
        return mViewHolder.getAdapterPosition();
    }
}