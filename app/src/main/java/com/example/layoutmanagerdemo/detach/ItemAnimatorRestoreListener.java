package com.example.layoutmanagerdemo.detach;

class ItemAnimatorRestoreListener implements ItemAnimator.ItemAnimatorListener {
    private RecyclerView recyclerView;

    ItemAnimatorRestoreListener(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Override
    public void onAnimationFinished(ViewHolder item) {
        item.setIsRecyclable(true);
        if (item.mShadowedHolder != null && item.mShadowingHolder == null) { // old vh
            item.mShadowedHolder = null;
        }
        item.mShadowingHolder = null;
        if (!item.shouldBeKeptAsChild()) {
            if (!recyclerView.removeAnimatingView(item.itemView) && item.isTmpDetached()) {
                recyclerView.removeDetachedView(item.itemView, false);
            }
        }
    }
}