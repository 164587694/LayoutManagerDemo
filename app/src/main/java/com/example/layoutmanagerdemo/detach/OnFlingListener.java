package com.example.layoutmanagerdemo.detach;

public abstract class OnFlingListener {
    public abstract boolean onFling(int velocityX, int velocityY);
}