package com.example.layoutmanagerdemo.detach;

import androidx.annotation.NonNull;

public abstract class OnScrollListener {
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
    }

    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
    }
}