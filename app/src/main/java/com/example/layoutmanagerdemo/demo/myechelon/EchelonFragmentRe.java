package com.example.layoutmanagerdemo.demo.myechelon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.layoutmanagerdemo.R;
import com.example.layoutmanagerdemo.detach.Adapter;
import com.example.layoutmanagerdemo.detach.RecyclerView;
import com.example.layoutmanagerdemo.detach.ViewHolder;

/**
 * Created by 钉某人
 * github: https://github.com/DingMouRen
 * email: naildingmouren@gmail.com
 * 梯形布局
 */
public class EchelonFragmentRe extends Fragment {
    private RecyclerView mRecyclerView;
    private ReEchelonLayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_echelon_re, container, false);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    private void initData() {
        mLayoutManager = new ReEchelonLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new MyAdapter());

    }

    class MyAdapter extends Adapter<MyAdapter.MyViewHolder> {
        private int[] icons = {R.mipmap.header_icon_1, R.mipmap.header_icon_2, R.mipmap.header_icon_3, R.mipmap.header_icon_4};
        private int[] bgs = {R.mipmap.bg_1, R.mipmap.bg_2, R.mipmap.bg_3, R.mipmap.bg_4};
        private String[] nickNames = {"左耳近心", "凉雨初夏", "稚久九栀", "半窗疏影"};
        private String[] descs = {
                "回不去的地方叫故乡 没有根的迁徙叫流浪...",
                "人生就像迷宫，我们用上半生找寻入口，用下半生找寻出口",
                "原来地久天长，只是误会一场",
                "不是故事的结局不够好，而是我们对故事的要求过多",
                "只想优雅转身，不料华丽撞墙"
        };

        public MyAdapter() {
            setHasStableIds(true);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_echelon, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.icon.setImageResource(icons[position % 4]);
            holder.nickName.setText(nickNames[position % 4]);
            holder.desc.setText(descs[position % 5]);
            holder.bg.setImageResource(bgs[position % 4]);
        }

        @Override
        public int getItemCount() {
            return 6000;
        }

        public class MyViewHolder extends ViewHolder {
            ImageView icon;
            ImageView bg;
            TextView nickName;
            TextView desc;

            public MyViewHolder(View itemView) {
                super(itemView);
                icon = itemView.findViewById(R.id.img_icon);
                bg = itemView.findViewById(R.id.img_bg);
                nickName = itemView.findViewById(R.id.tv_nickname);
                desc = itemView.findViewById(R.id.tv_desc);

            }
        }
    }

}
