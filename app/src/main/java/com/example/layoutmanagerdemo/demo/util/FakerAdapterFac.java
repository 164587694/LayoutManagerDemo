package com.example.layoutmanagerdemo.demo.util;

import android.content.Context;
import android.os.Handler;

import com.example.layoutmanagerdemo.R;
import com.example.layoutmanagerdemo.demo.adapter.ImageAdapter;
import com.example.layoutmanagerdemo.demo.adapter.StringAdapter;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by wangxingsheng on 2020/6/9.
 * desc:
 */
public class FakerAdapterFac {
    public static ImageAdapter<String> createFaker(Context context, int count) {
        List<String> data = new ArrayList<>();
        ImageAdapter<String> adapter = new ImageAdapter<>(data, context);
        backRun(() -> {
            for (int i = 0; i < count; i++) {
                data.add(Faker.instance(Locale.CHINESE).avatar().image());
            }
            new Handler(context.getMainLooper()).post(adapter::notifyDataSetChanged);
        });
        return adapter;
    }

    public static ImageAdapter<Integer> createLocalFaker(Context context, int count) {
        List<Integer> data = new ArrayList<>();
        ImageAdapter<Integer> adapter = new ImageAdapter<>(data, context);
        backRun(() -> {
            int[] icons = {R.mipmap.header_icon_1, R.mipmap.header_icon_2, R.mipmap.header_icon_3, R.mipmap.header_icon_4, R.mipmap.header_icon_5, R.mipmap.header_icon_6, R.mipmap.header_icon_7, R.mipmap.header_icon_8, R.mipmap.header_icon_9, R.mipmap.header_icon_10, R.mipmap.header_icon_11, R.mipmap.header_icon_12, R.mipmap.header_icon_13};
            for (int i = 0; i < count; i++) {
                data.add(icons[i % 13]);
            }
            new Handler(context.getMainLooper()).post(adapter::notifyDataSetChanged);
        });
        return adapter;
    }

    public static StringAdapter createStrFaker(Context context, int count) {
        List<String> data = new ArrayList<>();
        StringAdapter adapter = new StringAdapter(data, context);
        backRun(() -> {
            for (int i = 0; i < count; i++) {
                data.add("[" + i + "]-" + Faker.instance(Locale.CHINESE).name().username());
            }
            new Handler(context.getMainLooper()).post(adapter::notifyDataSetChanged);
        });
        return adapter;
    }

    private static void backRun(Runnable runnable) {
        new Thread(runnable).start();
    }
}
