package com.example.layoutmanagerdemo.demo.layoutmanagergroup;

import android.content.Context;

import com.reone.talklibrary.TalkApp;


/**
 * Created by 钉某人
 * github: https://github.com/DingMouRen
 * email: naildingmouren@gmail.com
 */

public class MyApplication extends TalkApp {
    public static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }
}
