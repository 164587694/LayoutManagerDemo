package com.example.layoutmanagerdemo.demo.adapter;

/**
 * Created by wangxingsheng on 2020/6/9.
 * desc:
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.layoutmanagerdemo.R;
import com.reone.talklibrary.TalkApp;

import java.util.List;

public class StringAdapter extends ItemClickAdapter<String, StringAdapter.StringViewHolder> {
    private Context context;
    private List<String> data;

    public StringAdapter(List<String> data, Context context) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public StringViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StringViewHolder(LayoutInflater.from(context).inflate(R.layout.item_string_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final StringViewHolder holder, int position) {
        String item = data.get(position);
        holder.textView.setText(item);
        attachClick(holder.itemView, item, holder.getAdapterPosition(), () -> TalkApp.talk(holder.getAdapterPosition() + " : " + item));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class StringViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView textView;

        public StringViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_text);
        }
    }
}
