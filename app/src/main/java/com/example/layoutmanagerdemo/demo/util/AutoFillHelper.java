package com.example.layoutmanagerdemo.demo.util;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import com.example.layoutmanagerdemo.R;

/**
 * Created by wangxingsheng on 2020/6/10.
 * desc:
 */
public class AutoFillHelper {

    public static void fillPageByRecycleView(Activity activity, RecyclerView.LayoutManager layoutManager, RecyclerView.Adapter<?> adapter) {
        activity.setContentView(R.layout.layout_recycle_view);
        RecyclerView recyclerView = activity.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}