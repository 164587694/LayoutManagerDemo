package com.example.layoutmanagerdemo.demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.layoutmanagerdemo.R;

import java.util.List;

public class ImageAdapter<T> extends ItemClickAdapter<T, ImageAdapter.ImageViewHolder> {
    private Context context;
    private List<T> data;

    public ImageAdapter(List<T> data, Context context) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_image_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, int position) {
        T item = data.get(position);
        Glide.with(context).load(item).into(holder.imageView);
        attachClick(holder.imageView, item, holder.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
        }
    }
}
