package com.example.layoutmanagerdemo.demo;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.layoutmanagerdemo.R;
import com.example.layoutmanagerdemo.demo.ac.NotifyCardActivity;
import com.example.layoutmanagerdemo.demo.ac.PathDemoActivity;
import com.example.layoutmanagerdemo.demo.ac.PathDemoActivity2;
import com.example.layoutmanagerdemo.demo.adapter.ImageAdapter;
import com.example.layoutmanagerdemo.demo.layoutmanagergroup.LayoutManagerGroupMainActivity;
import com.example.layoutmanagerdemo.demo.util.FakerAdapterFac;
import com.example.layoutmanagerdemo.layoutmanger.DemoCustomLayoutManager;
import com.reone.talklibrary.TalkApp;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageAdapter<Integer> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_s);
        recyclerView = findViewById(R.id.recycler_view);
        init();

    }

    private void init() {
        adapter = FakerAdapterFac.createLocalFaker(this, 20);
        adapter.setOnItemClickListener(((item, position) -> {
            TalkApp.talk("pos:" + position + " url:" + item);
            switch (position) {
                case 0:
                    startActivity(new Intent(this, PathDemoActivity.class));
                    break;
                case 1:
                    startActivity(new Intent(this, PathDemoActivity2.class));
                    break;
                case 2:
                    startActivity(new Intent(this, LayoutManagerGroupMainActivity.class));
                    break;
                case 3:
                    startActivity(new Intent(this, NotifyCardActivity.class));
                    break;
            }
        }));
        recyclerView.setLayoutManager(new DemoCustomLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
}
