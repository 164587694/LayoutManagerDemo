package com.example.layoutmanagerdemo.demo.ac;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.layoutmanagerdemo.R;
import com.example.layoutmanagerdemo.layoutmanger.me.NotifyCardLayoutManager;
import com.github.javafaker.Cat;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.Locale;

public class NotifyCardActivity extends AppCompatActivity implements View.OnClickListener {
    private int[] icons = {R.mipmap.header_icon_1, R.mipmap.header_icon_2, R.mipmap.header_icon_3, R.mipmap.header_icon_4, R.mipmap.header_icon_5, R.mipmap.header_icon_6, R.mipmap.header_icon_7, R.mipmap.header_icon_8, R.mipmap.header_icon_9, R.mipmap.header_icon_10, R.mipmap.header_icon_11, R.mipmap.header_icon_12, R.mipmap.header_icon_13};
    private RecyclerView recyclerView;
    private ArrayList<ItemBean> itemBeans = new ArrayList<>();
    private static final int MAX_COUNT = 5;
    private DemoItemAdapter adapter;
    private NotifyCardLayoutManager notifyCardLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_card);
        recyclerView = findViewById(R.id.recycler_view);
        findViewById(R.id.btn_add).setOnClickListener(this);
        findViewById(R.id.btn_less).setOnClickListener(this);
        findViewById(R.id.btn_add_bo).setOnClickListener(this);
        findViewById(R.id.btn_less_bo).setOnClickListener(this);
        initRecycleView();
    }

    private void initRecycleView() {
        notifyCardLayoutManager = new NotifyCardLayoutManager(5);
        notifyCardLayoutManager.setOnItemRemoveListener(position -> {
            Toast.makeText(this, "移除了" + itemBeans.get(position).name, Toast.LENGTH_SHORT).show();
            itemBeans.remove(position);
            adapter.notifyItemRemoved(position);
        });
        recyclerView.setLayoutManager(notifyCardLayoutManager);
        adapter = new DemoItemAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                int insert = getMidIndex();
                itemBeans.add(insert, createItemBean());
                adapter.notifyItemInserted(insert);
                break;
            case R.id.btn_less:
                if (itemBeans.size() > 0) {
                    int remove = getMidIndex();
                    itemBeans.remove(getMidIndex());
                    adapter.notifyItemRemoved(remove);
                }
                break;
            case R.id.btn_add_bo:
                itemBeans.add(createItemBean());
                adapter.notifyItemInserted(itemBeans.size() - 1);
                break;
            case R.id.btn_less_bo:
                if (itemBeans.size() > 0) {
                    itemBeans.remove(itemBeans.size() - 1);
                    adapter.notifyItemRemoved(itemBeans.size());
                }
                break;
        }
    }

    private int tempIndex = 0;

    private ItemBean createItemBean() {
        Cat cat = Faker.instance().cat();
        ItemBean itemBean = new ItemBean();
        itemBean.name = cat.name();
        itemBean.breed = cat.breed();
        itemBean.registry = cat.registry();
        itemBean.img = icons[tempIndex % icons.length];
        tempIndex++;
        return itemBean;
    }

    private int getMidIndex() {
        int size = itemBeans.size();
        int index = size - (Math.min(size, MAX_COUNT) / 2 + 1);
        if (index >= 0 && index < size) {
            return index;
        } else {
            return 0;
        }
    }

    private static class ItemBean {
        String name;
        String breed;
        String registry;
        int img;
    }

    private class DemoItemAdapter extends RecyclerView.Adapter<DemoItemViewHolder> {

        @NonNull
        @Override
        public DemoItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new DemoItemViewHolder(LayoutInflater.from(NotifyCardActivity.this).inflate(R.layout.item_notify_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull final DemoItemViewHolder holder, int position) {
            ItemBean item = itemBeans.get(position);
            holder.imageView.setImageResource(item.img);
            holder.textView.setText(String.format(Locale.CHINA, "[%d]%s", holder.getAdapterPosition(), item.name));
            holder.textView2.setText(item.breed);
            holder.textView3.setText(item.registry);
            holder.itemView.setOnClickListener(v -> {
                if (holder.getAdapterPosition() == itemBeans.size() - 1) {
                    Toast.makeText(NotifyCardActivity.this, item.name, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return itemBeans.size();
        }
    }

    static class DemoItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private TextView textView2;
        private TextView textView3;

        public DemoItemViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            textView2 = itemView.findViewById(R.id.textView2);
            textView3 = itemView.findViewById(R.id.textView3);
        }
    }
}