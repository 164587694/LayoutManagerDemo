package com.example.layoutmanagerdemo.demo.ac;

import android.graphics.Path;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.layoutmanagerdemo.R;
import com.example.layoutmanagerdemo.demo.util.FakerAdapterFac;
import com.example.layoutmanagerdemo.layoutmanger.path.PathLayoutManager;
import com.example.layoutmanagerdemo.layoutmanger.path.ShowPathView;

/**
 * Created by wangxingsheng on 2020/6/9.
 * desc:
 */
public class PathDemoActivity2 extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ShowPathView showPathView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_demo);
        showPathView = findViewById(R.id.show_path_view);
        recyclerView = findViewById(R.id.recycler_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initPathList();
    }

    private void initPathList() {
        Path path = new Path();
        path.moveTo(250, 250);
        path.rLineTo(600, 300);
        path.rLineTo(-600, 300);
        path.rLineTo(600, 300);
        path.rLineTo(-600, 300);
        showPathView.showPath(path);
        recyclerView.setLayoutManager(new PathLayoutManager(path, 200));
        recyclerView.setAdapter(FakerAdapterFac.createStrFaker(this, 20));
    }
}
