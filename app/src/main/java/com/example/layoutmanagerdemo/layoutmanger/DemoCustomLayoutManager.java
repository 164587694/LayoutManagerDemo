package com.example.layoutmanagerdemo.layoutmanger;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * Created by wangxingsheng on 2020/6/4.
 * desc:
 */
public class DemoCustomLayoutManager extends LinearLayoutManager {

    public DemoCustomLayoutManager(Context context) {
        super(context);
    }

    public DemoCustomLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public DemoCustomLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
