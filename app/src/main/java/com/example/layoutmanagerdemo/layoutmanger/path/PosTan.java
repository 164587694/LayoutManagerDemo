package com.example.layoutmanagerdemo.layoutmanger.path;

import android.graphics.PointF;

/**
 * Created by wangxingsheng on 2020/6/8.
 * desc:
 */
public class PosTan extends PointF {
    /**
     * 在路径上的位置 (百分比)
     */
    public float fraction;

    /**
     * Item所对应的索引
     */
    public int index;

    /**
     * Item的旋转角度
     */
    private float angle;

    public PosTan() {
    }

    public PosTan(PosTan posTan, int index, float fraction) {
        set(posTan.x, posTan.y, posTan.angle);
        setIndex(index);
        setFraction(fraction);
    }

    public void set(float x, float y, float angle) {
        set(x, y);
        this.angle = angle;
    }

    public float getFraction() {
        return fraction;
    }

    public void setFraction(float fraction) {
        this.fraction = fraction;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}

