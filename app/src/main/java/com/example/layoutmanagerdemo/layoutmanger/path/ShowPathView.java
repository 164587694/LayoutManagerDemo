package com.example.layoutmanagerdemo.layoutmanger.path;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * Created by wangxingsheng on 2020/6/9.
 * desc:
 */
public class ShowPathView extends View {
    private Path path;
    private Paint paint;

    public ShowPathView(Context context) {
        super(context);
    }

    public ShowPathView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ShowPathView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void showPath(Path path) {
        this.paint = new Paint();
        this.paint.setColor(Color.RED);
        this.paint.setStrokeWidth(10);
        this.paint.setStyle(Paint.Style.STROKE);
        this.path = path;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(path, paint);
    }
}
